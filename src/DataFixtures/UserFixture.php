<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixture extends Fixture
{

    private $passwordEncoder;

     public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
         $this->passwordEncoder = $passwordEncoder;
     }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setEmail('testuser@gmail.com');
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            'testuser'));
        $user->setNom('jean');
        $user->setPrenom('paul');
        $user->setAdresse('rue test');
        $user->setVille('Paris');
        $user->setCodePostal('75001');
        $user->setPays('France');
        $manager->persist($user);
        $manager->flush();
    }
}
