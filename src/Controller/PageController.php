<?php

namespace App\Controller;

use App\Form\UserType;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class PageController extends AbstractController
{

    /**
     * @var UserManager
     */
    private $em;

    private $passwordEncoder;

    /**
     * ProfilController constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->em = $em;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @Route("/user/", name="app_homepage")
     */
    public function index(): Response
    {
        return $this->render('page/index.html.twig', [
            'controller_name' => 'PageController',
        ]);
    }


    /**
     * @Route("/inscription", name="app_user_add")
     */
    public function addUser(Request $request): Response
    {
        $user = new User();
        $this->em->persist($user);
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                $form->getData()->getPassword())
            );
            $this->em->flush();
            return $this->redirectToRoute('app_login');

        }

        return $this->render('page/inscription.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/user/users", name="app_user_list")
     */
    public function listUser(Request $request): Response
    {
        $users = $this->em->getRepository(User::class)->findAll();


        return $this->render('page/users.html.twig', [
            'users' => $users
        ]);
    }


}
